<?php

require_once("dati.php");
require_once("functions.php");

global $debugger;
global $db;

$dsn = 'mysql:dbname='.DB_NAME.';host=127.0.0.1';


$db = new PDO($dsn, "root", "");

$debugger  = 0;

$id_scelti = array();
$level     = $_REQUEST["level"];

$lang      = $_SESSION["user_lang"];

// debug con scelta di attori .. non ancora attiva
$seq       = $_REQUEST["seq"];
if ($seq) {
    $attori_seq = explode(",", $seq);
    $numseq     = count($attori_seq);
}
////
if (!$level) {
    $level = 1;
}
// per ogni livello definisce il numero di attori e i gruppi
switch ($level) {
    case 1:
        $num_att = 8;
        $gruppo  = "0";
        break;
    case 2:
        $num_att = 10;
        $gruppo  = "0";
        break;
    case 3:
        $num_att = 12;
        $gruppo  = "0,1";
        break;
    case 4:
        $num_att = 14;
        $gruppo  = "0,1";
        break;
    case 5:
        $num_att = 16;
        $gruppo  = "0,1,2";
        break;
    case 6:
        $num_att = 18;
        $gruppo  = "0,1,2";
        break;
    case 7:
        $num_att = 20;
        $gruppo  = "0,1,2,3";
        break;
    case 8:
        $num_att = 22;
        $gruppo  = "0,1,2,3";
        break;
    case 9:
        $num_att = 24;
        $gruppo  = "0,1,2,3,4";
        break;
    default:
        $num_att = 24;
        $gruppo  = "0,1,2,3,4,5";
        break;
}
if ($_REQUEST["multiplayer"] == 1) {
    $num_att = 12;
    $gruppo  = "0,1,2";
    $level   = 3;
}
// $db = new PDO($dsn, DB_USER, DB_PSWD);
// parte con attori aggiunti in automatico
$y            = 0;
$m            = 0;
$esclusi      = 0;
$listatessere = array();
for ($w = 0; $w < $num_att / 2; $w++) {
    // crea un array con i numeri delle tessere che poi verranno rimescolati
    $listatessere[$w]     = $w;
    // query per l'attore a caso
    if(!$_REQUEST["squadra"]) {
    $lista                = creaCoppia($gruppo, $esclusi, $db);
    } else {
    $lista                = creaCoppiaSquadre($_REQUEST["squadra"], $esclusi, $db);
    }
    $att[$lista[0]["id"]] = $lista[1]["id"];

    $att[$lista[1]["id"]] = $lista[0]["id"];


    testa("coppia trovata", $lista, "green");

    $esclusi .= "," . $lista[0]["id"] . "," . $lista[1]["id"];

    testa("esclusi", $esclusi,"");
    // mette l'attore scelto random su un indice pari (aumenta di 2)
    $id_scelti["id"][$y]   = $lista[0]["id"];
    $id_scelti["nome"][$y] = $lista[0]["cognome"];
    // il primo lo mette in fondo poi riparte dall'inizio
    if ($y == 0) {
        $k = $num_att - 1;
    } else {
        $k = $y - 1;
    }
    $id_scelti["id"][$k]   = $lista[1]["id"];
    $id_scelti["nome"][$k] = $lista[1]["cognome"];
    $y                     = $y + 2;
}
///////// FINE SCELTA ATTORI

testa("fine scelta attori: ", $id_scelti, "black");

$x        = 0;
/***********  cambio ordine all'interno di ogni tessera: il numero di tessere che cambia e' random *****/
$numcambi = rand(0, $num_att / 2);

for ($x = 0; $x <= $numcambi; $x++) {
    // sceglie quale cambiare
    $cambio = rand(0, $num_att - 1);
    //	echo "cambio:".$random;
    // se cambia una pari
    if ($cambio % 2 == 0) {
        // pari
        $par_id                         = $id_scelti["id"][$cambio];
        $par_nome                       = $id_scelti["nome"][$cambio];
        // mette la pari dispari
        $id_scelti["id"][$cambio]       = $id_scelti["id"][$cambio + 1];
        $id_scelti["nome"][$cambio]     = $id_scelti["nome"][$cambio + 1];
        // la dispari pari
        $id_scelti["id"][$cambio + 1]   = $par_id;
        $id_scelti["nome"][$cambio + 1] = $par_nome;
    } else {
        // dispari
        $par_id                         = $id_scelti["id"][$cambio - 1];
        $par_nome                       = $id_scelti["nome"][$cambio - 1];
        $id_scelti["id"][$cambio - 1]   = $id_scelti["id"][$cambio];
        $id_scelti["nome"][$cambio - 1] = $id_scelti["nome"][$cambio];
        $id_scelti["id"][$cambio]       = $par_id;
        $id_scelti["nome"][$cambio]     = $par_nome;
    }
}
/***********************  rimescola la lista delle tessere  ******/ ///
// mescola l'ordine delle tessere
shuffle($listatessere);

// archivia gli idscelti che poi verranno cambiati
$idsceltivecchi = $id_scelti;
// cambio ordine tessere

// cancella gli id_scelti prima di cambiarli
unset($id_scelti);

$z = 0;
// per ogni tessera
for ($w = 0; $w < $num_att / 2; $w++) {

    // prende gli attori relativi alla tessera secondo gli ordini nuovi
    $numatt1               = $listatessere[$w] * 2;
    $numatt2               = ($listatessere[$w] * 2) + 1;
    $y                     = $z + 1;

    // li mette in nuovo ordine
    $id_scelti["id"][$z]   = $idsceltivecchi["id"][$numatt1];
    $id_scelti["id"][$y]   = $idsceltivecchi["id"][$numatt2];
    $id_scelti["nome"][$z] = $idsceltivecchi["nome"][$numatt1];
    $id_scelti["nome"][$y] = $idsceltivecchi["nome"][$numatt2];
    $z                     = $z + 2;
}

testa("id scelti dopo il mescolamento", $id_scelti, "black");
if ($debugger == 1) {
    echo "<div style='color:red'>";
}
$id_scelti    = utf8_converter($id_scelti);
// salva gli id_scelti per scriverli sul file nel multigame
$txt_idscelti = json_encode($id_scelti);
echo json_encode($id_scelti);

if ($debugger == 1) {
    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            echo ' - No errors';
            break;
        case JSON_ERROR_DEPTH:
            echo ' - Maximum stack depth exceeded';
            break;
        case JSON_ERROR_STATE_MISMATCH:
            echo ' - Underflow or the modes mismatch';
            break;
        case JSON_ERROR_CTRL_CHAR:
            echo ' - Unexpected control character found';
            break;
        case JSON_ERROR_SYNTAX:
            echo ' - Syntax error, malformed JSON';
            break;
        case JSON_ERROR_UTF8:
            echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
            break;
        default:
            echo ' - Unknown error';
            break;
    }
}
//print_r($att);
// $agganci=array();
echo "|";
testa("Terza fase: cerca gli agganci successivi tra i selezionati", "", "black");
for ($x = 0; $x < count($id_scelti["id"]); $x++) {
    $id = $id_scelti["id"][$x];

   testa("", $x, "green");

    $agganci["id"][$x] = $id;
    //  echo "<b>".$attore["cognome"]."</b>";
    // la terza query prende gli agganci di un attore con gli altri id selezionati

    if($_REQUEST["game"]=="goleador") {

      $sql3 = "SELECT " . COPPIE . "." . FKID_ATTORE . "2 as id2, coppie_int.squadra as titolo FROM " . COPPIE . " WHERE " . COPPIE . "." . FKID_ATTORE . "1=" . $id . " AND " . COPPIE . "." . FKID_ATTORE . "2 IN (" . $esclusi . ")";


    }  else  {

    $sql3 = "SELECT " . COPPIE . "." . FKID_ATTORE . "2 as id2, " . EL_FILM . " FROM " . COPPIE . " INNER JOIN " . FILM . " ON " . FILM . ".id=" . COPPIE . "." . FKID_FILM . " INNER JOIN " . ATTORI . " ON " . ATTORI . ".id=" . COPPIE . "." . FKID_ATTORE . "2 WHERE " . COPPIE . "." . FKID_ATTORE . "1=" . $id . " AND " . COPPIE . "." . FKID_ATTORE . "2 IN (" . $esclusi . ")";
    }

    testa("sql3", $sql3, "black");
    $result=$db->query($sql3);
    $lista2 = $result->fetchAll();

  //  $totagg = $db->sql_list_assoc($sql3, $lista2, $seek_start = 0, $seek_num = 0, $debug = 0);
  $totagg=count($lista2);
    // echo $id_scelti["nome"][$x]."</b><br/>";
    testa("lista2", $lista2, "black");
    $z = 1;

    testa("", $att, "blue");

    //aggancio selezionato nella seconda query (quello verde);
    $id2first = $att[$id];
    for ($m = 0; $m < $totagg; $m++) {
        $id2 = $lista2[$m]["id2"];

        if ($id2first == $id2) {
            $agganci['id2'][$x][0] = $lista2[$m]["id2"];
            if ($lang == "it") {
                $agganci['film'][$x][0] = $lista2[$m]["art"] . " " . $lista2[$m]["titolo"];
            } else {
                // se esiste il titolo originale (diverso dall'italiano base lo mette)
                if ($lista2[$m]["titolo_or"]) {
                    $agganci['film'][$x][0] = $lista2[$m]["art_or"] . " " . $lista2[$m]["titolo_or"];
                } else {
                    $agganci['film'][$x][0] = $lista2[$m]["art"] . " " . $lista2[$m]["titolo"];
                }
            }
        }
    }

    foreach ($lista2 as $attacco) {
        $id2 = $attacco["id2"];
        if ($lang == "it") {
            $art    = $attacco["art"];
            $titolo = $attacco["titolo"];
        } else {
            // se esiste l'originale è quello
            if ($attacco["titolo_or"]) {
                $titolo = $attacco["titolo_or"];
                $art    = $attacco["art_or"];
            } else {
                $titolo = $attacco["titolo"];
                $art    = $attacco["art"];
            }
        }
        if ($art != "L'" && $art != "l'" && $art !== "") {
            $art .= " ";
        }
        //echo "@@@@".$id2."@@@@@<br/>";
        if ($id2first == $id2) {
            // gli agganci id2 sono una serie di i2 che attaccano col primo
            $agganci['id2'][$x][0]  = $attacco["id2"];
            $agganci['film'][$x][0] = $art . $titolo." (".$attacco["anno"].")";
        } else {
            $agganci['id2'][$x][$z]  = $attacco["id2"];
            $agganci['film'][$x][$z] = $art . $titolo." (".$attacco["anno"].")";
            //echo $attacco["cognome"]."";
            //echo '{ "id2":'. $attacco["id2"].',';
            // echo '"film":"'.$attacco["art"].'&nbsp;'.$attacco["titolo"];
            //echo ."&nbsp;";
            //echo "<br/>";
            $z++;
        }
    }
    //ksort($agganci['titolo']);
    $lista2 = "";
}
testa("", $agganci, "orange");

$agganci = utf8_converter($agganci);

if ($debugger == 1) {
    echo "<div style='color:red'>";
}
echo json_encode($agganci, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
$txt_agganci = json_encode($agganci, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

// errori sul json
if ($debugger == 1) {
    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            echo ' - No errors';
            break;
        case JSON_ERROR_DEPTH:
            echo ' - Maximum stack depth exceeded';
            break;
        case JSON_ERROR_STATE_MISMATCH:
            echo ' - Underflow or the modes mismatch';
            break;
        case JSON_ERROR_CTRL_CHAR:
            echo ' - Unexpected control character found';
            break;
        case JSON_ERROR_SYNTAX:
            echo ' - Syntax error, malformed JSON';
            break;
        case JSON_ERROR_UTF8:
            echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
            break;
        default:
            echo ' - Unknown error';
            break;
    }
    echo "</div>";
}


if ($_REQUEST["multiplayer"] == 1) {
$np   = $_REQUEST["np"];
$nf   = "serpente" . $np . ".txt";
$file = fopen("mp/" . $_SESSION["gioco"] . "/RoomsClosed/" . $_SESSION["room"] . "/" . $nf, "w");
fwrite($file, $txt_idscelti . "|" . $txt_agganci);
fclose($file);
}
?>
