<?php
if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
    die('Accesso diretto non consentito.');
$game = $_REQUEST["game"];
global $game;
switch ($game) {
    case "goleador":
        define("ATTORI", "giocatori");
        define("ATTORIDATI", "giocatori.nome, giocatori.cognome, giocatori.soprannome");
        define("FILM", "squadre");
        define("UAS", "u_giocatori_stardust");
        DEFINE("COPPIE", "coppie_int");
        define("FKID_ATTORE", "fkid_giocatore");
        define("EL_FILM", "squadre.squadra as titolo");
        define("FKID_FILM", "fkid_squadra");
        define("GIOCO", 1);
        define("RUOLO", "");
        // database
        define("DB_NAME", 'goleador');
        define("DB_USER", 'root');
        define("DB_PSWD", '');
        define("DB_HOST", 'localhost');
        break;
    case "stardust":
        //database
        define("DB_USER", 'root');
        define("DB_PSWD", '');
        define("DB_HOST", '127.0.0.1');
        define("DB_NAME", 'playnew');
        define("ATTORI", "attori");
        define("ATTORIDATI", "attori.nome, attori.cognome");
        define("FILM", "film");
        define("UAS", "u_attori_stardust");
        define("COPPIE", "coppie");
        define("FKID_ATTORE", "fkid_attore");
        define("EL_FILM", "film.art, film.titolo, film.art_or, film.titolo_or, film.anno");
        define("FKID_FILM", "fkid_film");
        define("GIOCO", 1);
        define("RUOLO", "");
        break;
    case "trash":
        define('DB_USER', 'root');
        define('DB_PSWD', 'root');
        define('DB_NAME', 'movies');
        define('DB_HOST', 'localhost');
        define("ATTORI", "attori");
        define("ATTORIDATI", "attori.nome, attori.cognome");
        define("FILM", "film");
        define("UAS", "u_attori_stardust");
        define("FKID_ATTORE", "fkid_attore");
        //if($lang=="it") {
        //define(EL_FILM, "film.art, film.titolo");
        // } else {
        define(EL_FILM, "film.art, film.titolo, film.art_or, film.titolo_or");
        // }
        define(FKID_FILM, "fkid_film");
        DEFINE("COPPIE", "coppie_trash");
        define(GIOCO, 2);
        break;
    case "tv":
        //database
        define('DB_NAME', 'movies');
        define('DB_USER', 'root');
        define('DB_PSWD', '');
        define('DB_HOST', 'localhost');
        define("ATTORI", "attori");
        define("ATTORIDATI", "attori.nome, attori.cognome");
        define("FILM", "tv");
        define("UAS", "u_attori_stardust");
        define("COPPIE", "coppie_tv");
        define("FKID_ATTORE", "fkid_attore");
        define("EL_FILM", "tv.art, tv.titolo");
        define("FKID_FILM", "idtv");
        define("GIOCO", 31);
        define("RUOLO", "");
        break;
    case "cartoons":
        define('DB_NAME', 'cartoon_last');
        define('DB_USER', 'root');
        define('DB_PSWD', '');
        define('DB_HOST', 'localhost');
        define("ATTORI", "attori");
        if ($_SESSION["user_lang"] == "it") {
            define("ATTORIDATI", "attori.nome_ita as nome");
        } else {
            define("ATTORIDATI", "attori.nome_eng as nome");
        }
        define("FILM", "film");
        define("UAS", "u_attori_stardust");
        define("COPPIE", "coppieg");
        define("FKID_ATTORE", "fkid_attore");
        define("EL_FILM", "film.art, film.titolo, film.art_or, film.titolo_or");
        define("FKID_FILM", "fkid_film");
        define("GIOCO", 1);
        define("RUOLO", "AND coppieg.ruolo1 IS NULL AND coppieg.ruolo2 IS NULL");
}
?>
