<?php
include("dati.php");
global $debugger;
global $db;

/* crea le coppie attori - funzione ricorsiva */
function creaCoppia($gruppo, $esclusi, $db) {
    global $lista;
    global $debugger;
    // crea attore random
    $sql = "SELECT " . ATTORI . ".id, " . ATTORIDATI . " FROM " . ATTORI . " INNER JOIN " . UAS . " ON " . UAS . "." . FKID_ATTORE . "=" . ATTORI . ".id  WHERE " . UAS . ".fkid_stardust=" . GIOCO . " AND " . UAS . ".gruppo IN (" . $gruppo . ") AND " . ATTORI . ".id NOT IN (" . $esclusi . ") ORDER BY RAND() LIMIT 1";

    testa("Query random", $sql, "");
    $random = $db->sql_row($sql);
    testa("", $random, "");

    $id                  = $random["id"];
    $lista[0]["id"]      = $id;
    $lista[0]["cognome"] = $random["nome"] . " " . $random["cognome"];
    $esclusi .= "," . $id;
    // query per gli attori che legano
    $sql2 = "SELECT " . COPPIE . "." . FKID_ATTORE . "2 as fkid_attore2, " . ATTORIDATI . " FROM " . COPPIE . " INNER JOIN " . ATTORI . " ON " . ATTORI . ".id=" . COPPIE . "." . FKID_ATTORE . "2 WHERE " . FKID_ATTORE . "1=" . $id . " AND gruppo2 IN (" . $gruppo . ") AND " . FKID_ATTORE . "2  NOT IN (" . $esclusi . ") " . RUOLO . " ORDER BY RAND() LIMIT 1";

    testa("Query scelta:", $sql2, "");

    $scelto = $db->sql_row($sql2, $debug = 0);

    testa("Attore che lega col random: ".$id, $scelto, "");

    $lista[1]["id"]      = $scelto["fkid_attore2"];
    $lista[1]["cognome"] = $scelto["nome"] . " " . $scelto["cognome"];

    if (!$scelto) {
	      testa("Non ho trovato nulla rifaccio da capo", "", "red");
        // $lista=array();
        creaCoppia($gruppo, $esclusi, $db);
    }

    testa("fine funzione", $lista, "blue");

    return $lista;
}

/* testa valori se debugger = 1 */
function testa($titolo, $testo, $colore) {
    global $debugger;
    if ($debugger == 1) {
        if (is_array($testo)) {
            echo "<div style='color:" . $colore . "'>";
            echo "<b>" . $titolo . "</b><br/>";
            print_r($testo);
            echo "</div>";
        } else {
            echo "<div style='color:" . $colore . "'><b>" . $titolo . "</b><br/>" . $testo . "</div>";
        }
    }
}

function utf8_converter($array) {
    array_walk_recursive($array, function(&$item, $key)
    {
        $item = iconv('UTF-8', 'UTF-8//IGNORE', utf8_encode($item));

    });
    return $array;
}


?>
